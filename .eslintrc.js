module.exports = {
  extends: [
    'airbnb-base',
  ],
  plugins: [
    'import',
    'node',
    'react',
    'react-hooks',
  ],
  env: {
    browser: true,
    node: true,
    es6: true,
  },
  parserOptions: {
    ecmaVersion: 2021, // Set to the appropriate ES version you are using
  },
  rules: {
    // Add specific rules or overrides here
    // For example:
    // 'indent': ['error', 2],
    // 'quotes': ['error', 'single'],
    // 'semi': ['error', 'always'],
  },
};
