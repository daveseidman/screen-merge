# Screen Merge

## Development

Unforunately this is a difficult one to dev since it is best tested with an actual second screen, which is tricky over LAN so instead we simply run `npm run tunnel:client` and `npm run tunnel:server` to forward both the dev-server and socket-server over https. This is required for the server in order to handle secure socket connections and the client on the second screen in order to gain access to the camera. Otherwise we would be better suited to access the server from the clients by local IP
