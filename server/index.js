// const { createServer } = require('https');
const { createServer } = require('http');
const { Server } = require('socket.io');
const express = require('express');
const cors = require('cors');
const config = require('../config.json');

const origin = [
  config.client,
];

const app = express();
app.use(cors({ origin }));
// TODO: figure out a way to share this, .env file?
// const { client } = require('../src/config');

const server = createServer(app);
const io = new Server(server, { cors: { origin } });

app.get('/', (req, res) => {
  res.redirect(config.client);
});

io.on('connection', (socket) => {
  console.log('connected', socket.id);
  socket.on('color', (e) => {
    console.log(e.firstScreenID);
    io.to(e.firstScreenID).emit('color', e);
  });

  socket.on('secondScreenConnected', (e) => {
    console.log(`${e.secondScreenID} is connected to ${e.firstScreenID}`);
    io.to(e.firstScreenID).emit('secondScreenConnected', e.secondScreenID);
  });

  socket.on('disconnect', (e) => {
    console.log('send disconnect event to first screen for', socket.id);
  });
});

server.listen(8000, () => {
  console.log('app listening on port 8000');
});
