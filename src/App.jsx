import React, { useState } from 'react';
import FirstScreen from './views/FirstScreen'
import SecondScreen from './views/SecondScreen'
import isMobile from 'is-mobile';
import { io } from 'socket.io-client';
import { server } from '../config.json'

import './index.scss';

function App() {
  const [socket, setSocket] = useState({ id: null });
  console.log(`reach out to socket server here: ${server}`)
  const mySocket = io(server);
  mySocket.on('connect', () => {
    if (!socket.id) setSocket(mySocket);
    mySocket.on('secondScreenConnected', (data) => {
      console.log('second screen connected', data);
    })
  })
  // return () => {
  //   socket.current.off('connected');
  // }

  return (
    <div className='app'>
      <h1>Screen Merge</h1>
      <a href={server}>Visit this link to authorize this browser</a>
      {isMobile()
        ? <SecondScreen socket={socket} />
        : <FirstScreen socket={socket} />
      }
    </div>)
}
export default App;
