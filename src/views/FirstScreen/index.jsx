import React, { useEffect, useRef, useState } from "react";
import QRCode from "react-qr-code";
import { client } from '../../../config'

import './index.scss';

function FirstScreen(props) {
  const { socket } = props;
  const canvasRef = useRef();
  const context = useRef();
  const qrCodeRef = useRef();
  const [secondScreenID, setSecondScreenID] = useState(null);
  const [qrCodeValue, setQrCodeValue] = useState('');
  const [calibration, setCalibration] = useState('start');

  const width = 400;
  const height = 400;
  const messages = {
    'start': 'place your phone here',

  }

  useEffect(() => {
    context.current = canvasRef.current.getContext('2d');
  }, [canvasRef])

  useEffect(() => {
    if (socket.connected) {
      setQrCodeValue(`${client}/?id=${socket.id}`);
      socket.on('secondScreenConnected', (id) => {
        console.log('second screen connected', id);
        setSecondScreenID(id);
      })

      socket.on('color', ({ avgColor }) => {
        console.log('here')
        context.current.fillStyle = avgColor;
        context.current.fillRect(0, 0, width, height);
      })
    }
    return () => {

    }
  }, [socket.id])

  return (
    <div className="firstscreen">
      <h4>Visit this link on a mobile device or scan the QR code</h4>
      <p>first screen id: {socket.id}</p>

      {secondScreenID
        ? (
          <div className="firstscreen-pattern">
            <div className="firstscreen-pattern-phone">
              <p>{messages[calibration]}</p>
            </div>
          </div>
        )
        : (
          <div
            ref={qrCodeRef}
            className="firstscreen-qrcode"
          >
            <QRCode
              size={256}
              style={{ height: "auto", maxWidth: "100%", width: "100%" }}
              value={qrCodeValue}
              viewBox={`0 0 256 256`}
            />
          </div>
        )
      }
      <canvas
        width={width}
        height={height}
        ref={canvasRef}
      ></canvas>
    </div>
  )
}

export default FirstScreen;