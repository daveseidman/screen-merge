import React, { useEffect, useState, useRef } from 'react';
import { server } from '../../../config'
import './index.scss';

function SecondScreen(props) {
  const { socket } = props;
  const [stream, setStream] = useState(null);
  const [calibration, setCalibration] = useState(null);
  const videoRef = useRef();
  const canvasRef = useRef();
  const width = 64;
  const height = 32;
  const requestRef = useRef();
  const context = useRef();

  const params = new URLSearchParams(location.search);
  const firstScreenID = params.get('id');

  const calibrate = () => {

  }
  useEffect(() => {
    if (socket.id) socket.emit('secondScreenConnected', { firstScreenID, secondScreenID: socket.id })
    setCalibration('start');
  }, [socket.id])

  const render = () => {
    context.current.drawImage(videoRef.current, 0, 0, width, height);

    const imageData = context.current.getImageData(0, 0, width, height).data;
    let totalRed = 0;
    let totalGreen = 0;
    let totalBlue = 0;

    for (let i = 0; i < imageData.length; i += 4) {
      totalRed += imageData[i];
      totalGreen += imageData[i + 1];
      totalBlue += imageData[i + 2];
    }

    const numPixels = imageData.length / 4; // Each pixel is represented by 4 values (RGBA)
    const avgRed = totalRed / numPixels;
    const avgGreen = totalGreen / numPixels;
    const avgBlue = totalBlue / numPixels;

    // Display the average color
    const avgColor = `rgb(${avgRed.toFixed(0)}, ${avgGreen.toFixed(0)}, ${avgBlue.toFixed(0)})`;
    context.current.fillStyle = avgColor;
    context.current.fillRect(0, 0, width, height);
    if (socket) {
      socket.emit('color', { avgColor, firstScreenID })
    }

    requestRef.current = requestAnimationFrame(render);
  }

  const toggleCamera = () => {
    if (!stream) {
      navigator.mediaDevices.getUserMedia({ video: { width, height, facingMode: 'environment' } }).then(_stream => {
        setStream(_stream);
        videoRef.current.srcObject = _stream;
        requestRef.current = requestAnimationFrame(render);
      })
    } else {
      stream.getTracks().forEach((track) => {
        track.stop();
      })
      setStream(null);
      cancelAnimationFrame(requestRef.current);
    }
  }

  useEffect(() => {
    if (!context.current) context.current = canvasRef.current.getContext('2d');
  }, [])

  return (
    <div className="secondscreen">
      <h2>Second Screen</h2>
      <p>connect{calibration ? 'ed' : 'ing'} to {firstScreenID}</p>
      <div className='secondscreen-pattern'>
        <button
          type="button"
          disabled={!socket.id}
          onClick={toggleCamera}>
          {stream ? 'Stop Camera' : 'Start Camera'}
        </button>
        <video ref={videoRef}
          muted
          playsInline
          autoPlay
        />
        <canvas ref={canvasRef}
          width={width}
          height={height}
        />
        <button
          type="button"
          onClick={calibrate}
        >Calibrate</button>
      </div>
    </div>
  )
}

export default SecondScreen;